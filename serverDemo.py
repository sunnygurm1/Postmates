import keras as k
from keras.models import model_from_json
import random
import numpy as np
import matplotlib.pyplot as plt


def loadModel(jsonStr, weightStr):
    json_file = open(jsonStr, 'r')
    loaded_nnet = json_file.read()
    json_file.close()

    serve_model = model_from_json(loaded_nnet)
    # load weights into new model
    serve_model.load_weights(weightStr)

    # prep model for serving
    serve_model.compile(loss=k.losses.categorical_crossentropy,
                  optimizer=k.optimizers.RMSprop(),
                  metrics=['accuracy'])
    return serve_model

def inference(input):
    input = input.reshape(1, 28, 28, 1)
    inferenceVal = mod.predict(input)
    retVal = np.argmax(inferenceVal)
    return retVal
######
#edit the following line with appropriate paths: .json and .h5 filepaths
mod = loadModel('/home/sunny/PycharmProjects/Postmates/model/model.json', '/home/sunny/PycharmProjects/Postmates/model/model.h5')

# load the data
(x_train, y_train), (x_test, y_test) = k.datasets.mnist.load_data()

# scale data
x_test = x_test / 255


# randomly find a few index values between 0 and 9999
# then run inference on them
inds = []
for i in range(5):
    inds.append(random.randint(0,9999))
    plt.figure()
    plt.imshow(x_test[inds[i]])
    inferenceString = inference(x_test[inds[i]])
    plt.title("Predicted value: "+str(inferenceString)+", Actual value:"+str(y_test[inds[i]]))

