import numpy as np
import keras as k
from keras.layers import Input, Conv2D, MaxPooling2D, Dropout, Dense, Flatten
from keras.models import Model
from keras.utils.np_utils import to_categorical

(x_train, y_train), (x_test, y_test) = k.datasets.mnist.load_data()

#one hot encode the labels
y_train = to_categorical(y_train, num_classes=10)
y_test = to_categorical(y_test, num_classes=10)

#scale data between 0 and 1
x_train = x_train / 255
x_test  = x_test / 255


img_rows, img_cols = 28,28


x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols,1)
x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols,1)
k.initializers.glorot_uniform(seed=1)


inputMat = Input(shape=(28,28,1))
conv1 = Conv2D(32, kernel_size=(3,3), activation='relu', padding='same')(inputMat)
conv1 = MaxPooling2D(pool_size=(2,2))(conv1)

#branch 1 here
conv2_1 = Conv2D(64, kernel_size=(2,2), activation='relu', padding='same')(conv1)
conv2_1 = MaxPooling2D(pool_size=(2,2))(conv2_1)
conv3_1 = Conv2D(256, kernel_size=(2,2), activation='relu', padding='same')(conv2_1)

#branch2 here
conv2_2 = Conv2D(64, kernel_size=(2,2), activation='relu', padding='same')(conv1)
conv2_2 = MaxPooling2D(pool_size=(2,2))(conv2_2)
conv3_2 = Conv2D(256, kernel_size=(2,2), activation='relu', padding='same')(conv2_2)

#merge layers
merge_layer = k.layers.concatenate([conv3_1, conv3_2])
flatten_layer = Flatten()(merge_layer)
FCLayer_1 = Dense(1000, activation='relu')(flatten_layer)
FCLayer_1 = Dropout(0.5)(FCLayer_1)
FCLayer_2 = Dense(500, activation='relu')(FCLayer_1)
FCLayer_2 = Dropout(0.5)(FCLayer_2)
output_layer = Dense(10, activation='softmax')(FCLayer_2)


nnet = Model(inputs=inputMat, outputs=output_layer)

nnet.compile(loss=k.losses.categorical_crossentropy,
              optimizer=k.optimizers.RMSprop(),
              metrics=['accuracy'])

history=nnet.fit(x_train, y_train, batch_size=1024, epochs=9, shuffle=True, validation_data=(x_test, y_test))

# serialize model to JSON
model_json = nnet.to_json()
with open("/home/sunny/PycharmProjects/Postmates/model/model.json", "w") as json_file:
    json_file.write(model_json)

# serialize weights to HDF5
nnet.save_weights("/home/sunny/PycharmProjects/Postmates/model/model.h5")


