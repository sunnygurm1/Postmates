Info for funning the following Python files.

To run the demo files, please ensure you have the following packages:

1. Tensorflow and Keras
2. Matplotlib
3. Numpy

########## Running the Files ############
1. trainMNIST.py

Edit lines 60 and 64 to include the desired path where you'd like to save the model architecture and weights.
The pre-trained model is already in the subfolder 'model'

model.json - contains the model architecture
model.h5 - contains the mode

2. serverDemo.py

- This script loads the model architecture and weights, then runs an inference on 5 randomly selected values from the MNIST test set

- Please ensure that the path to the weights and architecture is correct (line 30) 

- Running the script will open new plots of the images used for inference; the title will contain the predicted vs. actual values (see sampleOutput.png)

- The line where the inference occurs is:

inferenceString = inference(x_test[inds[i]])


